
package Herencia;

/**
 *
 * @author zazue
 */
public class EmpleadoEventual extends Empleado {
    private float PagoHoras;
    private float hrsTrabajadas;

    public EmpleadoEventual(float PagoHoras, float hrsTrabajadas, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.PagoHoras = PagoHoras;
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    public EmpleadoEventual(){
        super();
        this.hrsTrabajadas = 0.0f;
        this.PagoHoras = 0.0f;
        
    }

    public float getPagoHoras() {
        return PagoHoras;
    }

    public void setPagoHoras(float PagoHoras) {
        this.PagoHoras = PagoHoras;
    }

    public float getHrsTrabajadas() {
        return hrsTrabajadas;
    }

    public void setHrsTrabajadas(float hrsTrabajadas) {
        this.hrsTrabajadas = hrsTrabajadas;
    }
    
    
    
    
    
    @Override
    public float calcularPago() {
     float pago = 0.0f;
     pago = this.hrsTrabajadas * this.PagoHoras;
     return pago;
    }
    
    
}
