package Herencia;
import javax.swing.JOptionPane;
import Herencia.jifEmpleadoBase;
public class jifEmpleadoBase extends javax.swing.JInternalFrame {
    public jifEmpleadoBase() {
        initComponents();
        this.desahabilitar();
   
    }
      
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtnumempleado = new javax.swing.JLabel();
        txtnombre = new javax.swing.JLabel();
        txtdepartamento = new javax.swing.JLabel();
        txtdiatrabajado = new javax.swing.JLabel();
        txtpuesto = new javax.swing.JLabel();
        txtpagodiario = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        txtsubtotal = new javax.swing.JLabel();
        txtimpuestos = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        jTextField7 = new javax.swing.JTextField();
        jTextField8 = new javax.swing.JTextField();
        jTextField9 = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Empleado Base");
        getContentPane().setLayout(null);

        txtnumempleado.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtnumempleado.setText("Num.Empleado");
        getContentPane().add(txtnumempleado);
        txtnumempleado.setBounds(34, 17, 119, 22);

        txtnombre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtnombre.setText("Nombre");
        getContentPane().add(txtnombre);
        txtnombre.setBounds(34, 54, 62, 30);

        txtdepartamento.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtdepartamento.setText("Departamento");
        getContentPane().add(txtdepartamento);
        txtdepartamento.setBounds(34, 126, 111, 30);

        txtdiatrabajado.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtdiatrabajado.setText("Dia Trabajado");
        getContentPane().add(txtdiatrabajado);
        txtdiatrabajado.setBounds(30, 190, 110, 30);

        txtpuesto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtpuesto.setText("Puesto");
        getContentPane().add(txtpuesto);
        txtpuesto.setBounds(34, 90, 53, 30);

        txtpagodiario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtpagodiario.setText("Pago Diario");
        getContentPane().add(txtpagodiario);
        txtpagodiario.setBounds(30, 240, 90, 30);

        jTextField1.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jTextField1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1);
        jTextField1.setBounds(295, 17, 130, 25);

        jTextField2.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jTextField2.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        getContentPane().add(jTextField2);
        jTextField2.setBounds(295, 58, 130, 25);

        jTextField3.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jTextField3.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        getContentPane().add(jTextField3);
        jTextField3.setBounds(295, 94, 130, 25);

        jTextField4.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jTextField4.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        getContentPane().add(jTextField4);
        jTextField4.setBounds(295, 130, 130, 25);

        jTextField5.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jTextField5.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        getContentPane().add(jTextField5);
        jTextField5.setBounds(300, 200, 120, 25);

        jTextField6.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jTextField6.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        getContentPane().add(jTextField6);
        jTextField6.setBounds(300, 250, 120, 30);

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculo de Pago de Nomina", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Book Antiqua", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N
        jPanel1.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("jLabel7");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(30, 60, 48, 21);

        jLabel8.setFont(new java.awt.Font("Cambria", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Sub total");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(30, 30, 56, 18);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(34, 306, 0, 0);

        jPanel2.setBackground(new java.awt.Color(0, 204, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Calculo de Pago de Nomina", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Constantia", 1, 12), new java.awt.Color(255, 255, 255))); // NOI18N

        txtsubtotal.setText("Sub Total");

        txtimpuestos.setText("Impuesto");

        txttotal.setText("Total a Pagar");

        jTextField8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txttotal)
                    .addComponent(txtimpuestos, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtsubtotal, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField7, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                    .addComponent(jTextField8)
                    .addComponent(jTextField9))
                .addContainerGap(305, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtsubtotal))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtimpuestos)
                    .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txttotal)
                    .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 60, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2);
        jPanel2.setBounds(250, 330, 590, 210);

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(630, 10, 140, 70);

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(630, 100, 140, 80);

        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(630, 200, 140, 90);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(20, 290, 130, 80);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(20, 390, 130, 80);

        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(20, 490, 130, 80);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTextField8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField8ActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        this.habilitar();
        this.txtnumempleado.requestFocus();
        

// TODO add your handling code here:
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if(this.validar()==true){
            //todo bien
            empleadoBase.setNumEmpleado(Integer.parseInt(this.txtnumempleado.getText()));
            empleadoBase.setNombre(this.txtnombre.getText());
            empleadoBase.setDepto(this.txtdepartamento.getText());
            empleadoBase.setPuesto(this.txtpuesto.getText());
            empleadoBase.setPagoDiario(Float.parseFloat(this.txtpagodiario.getText()));
            empleadoBase.setDiasTrabajados(Float.parseFloat(this.txtdiatrabajado.getText()));
            
            this.btnMostrar.setEnabled(true);
            JOptionPane.showMessageDialog(this, "Se guardo con exito");
            this.limpiar();
        }else
        {
            JOptionPane.showMessageDialog(this, "Faltaron datos por capturar");
        }
// TODO add your handling code here:
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        this.txtnumempleado.setText(String.valueOf(empleadoBase.getNumEmpleado()));
        this.txtnombre.setText(empleadoBase.getNombre());
        this.txtdepartamento.setText(empleadoBase.getDepto());
        this.txtpuesto.setText(empleadoBase.getPuesto());
        
        this.txtpagodiario.setText(String.valueOf(empleadoBase.getPagoDiario()));
        this.txtdiatrabajado.setText(String.valueOf(empleadoBase.getDiasTrabajados()));
        
        //calculos
        this.txtsubtotal.setText(String.valueOf(empleadoBase.calcularPago()));
        this.txtimpuestos.setText(String.valueOf(empleadoBase.calcularImpuestos()));
        this.txttotal.setText(String.valueOf(empleadoBase.calcularPago()));
// TODO add your handling code here:
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    this.limpiar();
// TODO add your handling code here:
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    this.limpiar();
    this.desahabilitar();
// TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int res = JOptionPane.showConfirmDialog(this, "Deseas salir","Terreno",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        
        if(res==JOptionPane.YES_OPTION)this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    private javax.swing.JLabel txtdepartamento;
    private javax.swing.JLabel txtdiatrabajado;
    private javax.swing.JLabel txtimpuestos;
    private javax.swing.JLabel txtnombre;
    private javax.swing.JLabel txtnumempleado;
    private javax.swing.JLabel txtpagodiario;
    private javax.swing.JLabel txtpuesto;
    private javax.swing.JLabel txtsubtotal;
    private javax.swing.JLabel txttotal;
    // End of variables declaration//GEN-END:variables
private EmpleadoBase empleadoBase = new EmpleadoBase();
    //helpers
    public void desahabilitar(){
        this.txtdepartamento.setEnabled(false);
        this.txtdiatrabajado.setEnabled(false);
        this.txtimpuestos.setEnabled(false);
        this.txtnombre.setEnabled(false);
        this.txtnumempleado.setEnabled(false);
        this.txtpagodiario.setEnabled(false);
        this.txtpuesto.setEnabled(false);
        this.txtsubtotal.setEnabled(false);
        this.txttotal.setEnabled(false);
        //botones
        this.btnMostrar.setEnabled(false);
        this.btnGuardar.setEnabled(false);
    }
    public void habilitar(){
        this.txtdepartamento.setEnabled(!false);
        this.txtdiatrabajado.setEnabled(!false);
        this.txtnombre.setEnabled(!false);
        this.txtnumempleado.setEnabled(!false);
        this.txtpagodiario.setEnabled(!false);
        this.txtpuesto.setEnabled(!false);
        
        this.btnGuardar.setEnabled(!false);
    }
    public void limpiar(){
        this.txtdepartamento.setText("");
        this.txtdiatrabajado.setText("");
        this.txtnombre.setText("");
        this.txtnumempleado.setText("");
        this.txtpagodiario.setText("");
        this.txtpuesto.setText("");
    }
    public boolean validar(){
        boolean exito= false;
        if(this.txtnumempleado.getText().equals("")||
        this.txtnombre.getText().equals("") ||
        this.txtdepartamento.getText().equals("") ||
        this.txtpuesto.getText().equals("") ||
        this.txtdiatrabajado.getText().equals("") ||
        this.txtpagodiario.getText().equals("")) exito = false;
        
        return exito;
    }
}