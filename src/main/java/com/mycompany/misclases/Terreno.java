
package com.mycompany.misclases;

/**
 *
 * @author zazue
 */
public class Terreno {
    private int numerodelote;
    private float largo;
    private float ancho;
    //constructores
public Terreno(){
    this.numerodelote=0;
    this.largo=0.0f;
    this.ancho=0.0f;
}
// condtructor copia
public Terreno(Terreno t){
    this.numerodelote=0;
    this.ancho=0.0f;
    this.largo=0.0f;
}
// constructor parametro
public Terreno(int numerodelote,float ancho,float largo){
    this.numerodelote= numerodelote;
    this.ancho= ancho;
    this.largo= largo;
}
//encapsulado
//encapsulamiento
    public int getnumerodelote(){
        return this.numerodelote;
    }
    public void setnumerodelote(int numerodelote){
        this.numerodelote = numerodelote;
    }
    public float getancho(){
        return this.ancho;
    }
    public void setancho(float ancho){
        this.ancho = ancho;
    }
    public float getlargo(){
        return this.largo;
    }
    public void setlargo(float largo){
        this.largo = largo;
    }
    //metodos de comportamiento
public float calcularPerimetro(){
return this.ancho * 2 + this.largo;
}
public float calcularArea(){
return this.ancho * this.largo;
    }
}
